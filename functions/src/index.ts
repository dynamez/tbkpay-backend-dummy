import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as express from "express";
import * as bodyParser from "body-parser";


admin.initializeApp(functions.config().firebase);

let globalResponseStatus = 200;
let globalEndpointSwitcher = "qr";
const cors = require('cors');
const app = express();
app.use(cors());
const main = express();
const db = admin.firestore();
main.use('/api/v1', app);
main.use(bodyParser.json());

export const testApi = functions.https.onRequest(main);

const CREATED_STATUS = "0001";
// const SCANNED_STATUS = "0002";
// const CONFIRMED_STATUS = "0003";
// const FINISHED_STATUS = "0004";
// const EXPIRED_STATUS = "0005";
// const CANCELED_STATUS = "0006";
// const ERROR_STATUS = "-0000";
//Wallet scans the QR and warns the backend
//service that updates the transaction status
app.post('/transaction/update', async (request, response) => {

    try {
        const {transactionid, transactionstatus} = request.body;
        const datoRef = await db.collection("transactions")
            .where("transactionId", "==", transactionid).get();
        const deviceData = await db.collection("devices")
            .where("deviceid", "==", datoRef.docs[0].data().deviceId).get();
        const dataUpdate = {
            status: transactionstatus
        };
        await db.collection("transactions").doc(transactionid).set(dataUpdate, {merge: true});

        const message = {
            notification: {
                title: "Transacciones TBKPay",
                body: "Transaccion ha cambiado de estado"
            },
            data: {
                "notificationType": "statusUpdate"
            },
            token: deviceData.docs[0].data().token
        };

        admin.messaging().send(message)
            // tslint:disable-next-line:no-shadowed-variable
            .then((response) => {
                // Response is a message ID string.
                console.log('Successfully sent message:', response);
                return null;   // add this line
            })
            .catch((error) => {
                console.log('Error sending message:', error);
            });
        response.json({
            data: "OK",
            status: transactionstatus
        });

    } catch (e) {
        console.log(e);
        response.status(500).send(e);
    }

});
//gets transaction data
app.get('/transaction/:transactionId/data', async (request, response) => {

    try {
        const transactionId = request.params.transactionId;
        console.log("transaction id: " + transactionId);
        response.json({
            //needs to be replaced with real data
            responseCode: "OK"
        });
    } catch (e) {
        response.status(500).send(e);
    }

});
//sends an email to user when transaction data
app.get('/transaction/:transactionId/sendemail', async (request, response) => {

    try {
        const transactionId = request.params.transactionId;
        console.log("transaction id: " + transactionId);
        response.json({
            responseCode: "OK"
        });
    } catch (e) {
        response.status(500).send(e);
    }

});
//ask current transaction status, passing a list of transaction otts
app.post('/transaction/status', async (request, response) => {
    try {

        console.log(request.body);
        const {transactions} = request.body;

        const datoRef = await db.collection("transactions")
            .get();
        const arr = [];

        datoRef.forEach(
            (doc) => {

                for (let item of transactions) {

                    if (item === doc.data().transactionId) {
                        const document = {
                            transaction_id: '',
                            ack_time: '',
                            qr_code_emv: '',
                            qr_code_id: '',
                            qr_time: '',
                            sale_amount: '',
                            tip_amount: '',
                            total_amount: '',
                            total_time: '',
                            transaction_time: '',
                            status: ''
                        };
                        document.transaction_id = doc.data().transactionId;
                        document.status = doc.data().status;
                        document.ack_time = doc.data().ACKTime;
                        document.qr_code_emv = doc.data().QRCodeEMV;
                        document.qr_code_id = doc.data().QRCodeId;
                        document.qr_time = doc.data().QRTime;
                        document.sale_amount = doc.data().amount;
                        document.tip_amount = doc.data().tipAmount;
                        document.total_amount = doc.data().totalAmount;
                        document.total_time = doc.data().totalTime;
                        document.transaction_time = doc.data().transactionTime;


                        arr.push(document);
                        console.log(arr)
                    }
                }

            }
        );
        response.status(200).json(arr)

    } catch (e) {
        console.log(e);
        response.status(500).send(e)
    }

});
app.get('/status/:transactionId', async (request, response) => {
    try {

        console.log(request.params.transactionId);
        const transactionId = request.params.transactionId;

        const datoRef = await db.collection("transactions")
            .where("transactionId", "==", transactionId)
            .get();
        console.log(datoRef);
        if (datoRef.size > 0) {
            const document = {
                transaction_id: '',
                ack_time: '',
                qr_code_emv: '',
                qr_code_id: '',
                qr_time: '',
                sale_amount: '',
                tip_amount: '',
                total_amount: '',
                total_time: '',
                transaction_time: '',
                status: ''
            };

            datoRef.forEach(
                (doc) => {

                    document.transaction_id = doc.data().transactionId;
                    document.status = doc.data().status;
                    document.ack_time = doc.data().ACKTime;
                    document.qr_code_emv = doc.data().QRCodeEMV;
                    document.qr_code_id = doc.data().QRCodeId;
                    document.qr_time = doc.data().QRTime;
                    document.sale_amount = doc.data().amount;
                    document.tip_amount = doc.data().tipAmount;
                    document.total_amount = doc.data().totalAmount;
                    document.total_time = doc.data().totalTime;
                    document.transaction_time = doc.data().transactionTime;

                }
            );
            //console.log(document);
            if (globalEndpointSwitcher === "recovery") {
                switch (globalResponseStatus) {
                    case 200:
                        response.status(200).send(document)
                        break;
                    case 404:
                        response.sendStatus(404)
                        break;
                    case 500:
                        response.sendStatus(500)
                        break;
                    default:
                        response.status(200).send(document)
                        break;
                }
            } else {
                response.status(200).send(document)
            }

        } else {
            response.sendStatus(404)
        }


    } catch (e) {
        console.log(e);
        response.status(500).send(e)
    }

});

// tslint:disable-next-line:no-shadowed-variable
app.get('/sales/history', async (request, response) => {
    try {
        const type = request.query.type;
        //const date = request.params.date;
        const page = request.query.page;
        //const limit = request.params.limit;
        const timer = ms => new Promise(res => setTimeout(res, ms));
        await timer(2000);
        if (globalEndpointSwitcher === "history") {
            switch (globalResponseStatus) {
                case 200:
                    if (type === "daily") {
                        switch (page) {
                            case "0":
                                response.status(200).json({
                                    "_embedded": {
                                        "sales_history": [
                                            {
                                                "transaction_id": "77TT2A5062K",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "VVVRL7CMWZI",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:00",
                                                "card_type": "Credit",
                                                "amount": 50000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "IQPPIL9VZH",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:07:00",
                                                "card_type": "Debit",
                                                "amount": 4500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "93PD38AGVWO",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:06:00",
                                                "card_type": "Credit",
                                                "amount": 3000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "KXAQY6ZJDV",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:05:00",
                                                "card_type": "Debit",
                                                "amount": 6500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "HFIMI967O5",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:04:00",
                                                "card_type": "Debit",
                                                "amount": 2500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "9HHQBA9ZS3T",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:03:00",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "B29MGFNIIXS",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:50",
                                                "card_type": "Debit",
                                                "amount": 9550,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "0S953DYE04LC",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:45",
                                                "card_type": "Debit",
                                                "amount": 1300,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "V7ODOSRXB3",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:35",
                                                "card_type": "Debit",
                                                "amount": 4500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "W5JQML8Y4YR",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:30",
                                                "card_type": "Debit",
                                                "amount": 15000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "EC34GDP1OZW",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:20",
                                                "card_type": "Debit",
                                                "amount": 950000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "QY6YB0JRV6G",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:15",
                                                "card_type": "Debit",
                                                "amount": 135000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "MK8B3TSCWRE",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:10",
                                                "card_type": "Debit",
                                                "amount": 7500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "WVX9REBPPT",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:03",
                                                "card_type": "Debit",
                                                "amount": 9800,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "BFHQPFK8W7T",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:01:55",
                                                "card_type": "Debit",
                                                "amount": 15600,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "H4243UGOBHN",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:01:20",
                                                "card_type": "Debit",
                                                "amount": 550000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "KZ73T10ZWZ",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:01:10",
                                                "card_type": "Debit",
                                                "amount": 98000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "5ZC1X2IVNHI",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:01:00",
                                                "card_type": "Debit",
                                                "amount": 7600,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "BQMLM296H8G",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:00:45",
                                                "card_type": "Debit",
                                                "amount": 89000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            }
                                        ],
                                        "total_sales_amount": 2169350
                                    },
                                    "_links": {
                                        "self": {
                                            "href": "/api/v1/sales/history?type=daily&page=0"
                                        },
                                        "first": {
                                            "href": "/api/v1/sales/history?type=daily"
                                        },
                                        "next": {
                                            "href": "/api/v1/sales/history?type=daily&page=1"
                                        },
                                        "last": {
                                            "href": "/api/v1/sales/history?type=daily&page=2"
                                        }
                                    },
                                    "page": {
                                        "size": 20,
                                        "total_elements": 60,
                                        "total_pages": 3
                                    }
                                })
                                break;

                            case "1":
                                response.status(200).json({
                                    "_embedded": {
                                        "sales_history": [
                                            {
                                                "transaction_id": "JQDECCKEQF",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "9ADLI08N8N6",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Credit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "X2270ECLM4B",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Credit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "NL3M5LDS8BN",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "SXJLTF1CH2E",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "5356IHY42CV",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "5G8ZJTHRX69",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "4NKPDGXDH8Q",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "BYDQVR69LDU",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "8F7GEPTPJRX",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "09V1L97PO53D",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "2Y8Y8S64A59",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "V999H90JSP",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "165FGHUK5ZT",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "UK7L8R0LBMH",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "30LUAAE8VLF",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "K4ZNDLAEFB9",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "OTA0A8BMBRN",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "VAX5PABWEM8",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "SX4ZL1S3IL",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            }
                                        ],
                                        "total_sales_amount": 2169350
                                    },
                                    "_links": {
                                        "self": {
                                            "href": "/api/v1/sales/history?type=daily&page=1"
                                        },
                                        "first": {
                                            "href": "/api/v1/sales/history?type=daily"
                                        },
                                        "prev": {
                                            "href": "/api/v1/sales/history?type=daily&page=0"
                                        },
                                        "next": {
                                            "href": "/api/v1/sales/history?type=daily&page=2"
                                        },
                                        "last": {
                                            "href": "/api/v1/sales/history?type=daily&page=2"
                                        }
                                    },
                                    "page": {
                                        "size": 20,
                                        "total_elements": 60,
                                        "total_pages": 3
                                    }
                                })
                                break;

                            case "2":
                                response.status(200).json({
                                    "_embedded": {
                                        "sales_history": [
                                            {
                                                "transaction_id": "SE5GTYT9EIK",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "2B21IU12VMH",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "VZ732B9QOJ",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "XVD8EPBLKL",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "LYTHNJU5JXN",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "VKE1J1RRMH9",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "PTGWQJKUOY",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "672SEB45WQU",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "NGC1ML3I99",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "DM5ZTMN4HJ8",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "P2TR3EH9MLR",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "4J1QZX7PIS8",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "AF8TXSB7ESF",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "DSEP98CC7G",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "AQZ6TA5QS2M",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "X0I82MQ6RLA",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "4I70ZDNX8MF",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "ZZB3RTRTQQ",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "WQKYJUZ82M",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "9W0HJWOELH",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            }
                                        ],
                                        "total_sales_amount": 2169350
                                    },
                                    "_links": {
                                        "self": {
                                            "href": "/api/v1/sales/history?type=daily&page=2"
                                        },
                                        "first": {
                                            "href": "/api/v1/sales/history?type=daily"
                                        },
                                        "prev": {
                                            "href": "/api/v1/sales/history?type=daily&page=1"
                                        },
                                        "last": {
                                            "href": "/api/v1/sales/history?type=daily&page=2"
                                        }
                                    },
                                    "page": {
                                        "size": 20,
                                        "total_elements": 60,
                                        "total_pages": 3
                                    }
                                });
                                break;

                            default:
                                response.status(200).json({
                                    "_embedded": {
                                        "sales_history": [
                                            {
                                                "transaction_id": "77TT2A5062K",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "VVVRL7CMWZI",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:00",
                                                "card_type": "Credit",
                                                "amount": 50000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "IQPPIL9VZH",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:07:00",
                                                "card_type": "Debit",
                                                "amount": 4500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "93PD38AGVWO",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:06:00",
                                                "card_type": "Credit",
                                                "amount": 3000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "KXAQY6ZJDV",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:05:00",
                                                "card_type": "Debit",
                                                "amount": 6500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "HFIMI967O5",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:04:00",
                                                "card_type": "Debit",
                                                "amount": 2500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "9HHQBA9ZS3T",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:03:00",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "B29MGFNIIXS",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:50",
                                                "card_type": "Debit",
                                                "amount": 9550,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "0S953DYE04LC",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:45",
                                                "card_type": "Debit",
                                                "amount": 1300,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "V7ODOSRXB3",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:35",
                                                "card_type": "Debit",
                                                "amount": 4500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "W5JQML8Y4YR",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:30",
                                                "card_type": "Debit",
                                                "amount": 15000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "EC34GDP1OZW",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:20",
                                                "card_type": "Debit",
                                                "amount": 950000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "QY6YB0JRV6G",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:15",
                                                "card_type": "Debit",
                                                "amount": 135000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "MK8B3TSCWRE",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:10",
                                                "card_type": "Debit",
                                                "amount": 7500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "WVX9REBPPT",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:03",
                                                "card_type": "Debit",
                                                "amount": 9800,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "BFHQPFK8W7T",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:01:55",
                                                "card_type": "Debit",
                                                "amount": 15600,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "H4243UGOBHN",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:01:20",
                                                "card_type": "Debit",
                                                "amount": 550000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "KZ73T10ZWZ",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:01:10",
                                                "card_type": "Debit",
                                                "amount": 98000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "5ZC1X2IVNHI",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:01:00",
                                                "card_type": "Debit",
                                                "amount": 7600,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "BQMLM296H8G",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:00:45",
                                                "card_type": "Debit",
                                                "amount": 89000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            }
                                        ],
                                        "total_sales_amount": 2169350
                                    },
                                    "_links": {
                                        "self": {
                                            "href": "/api/v1/sales/history?type=daily&page=0"
                                        },
                                        "first": {
                                            "href": "/api/v1/sales/history?type=daily"
                                        },
                                        "next": {
                                            "href": "/api/v1/sales/history?type=daily&page=1"
                                        },
                                        "last": {
                                            "href": "/api/v1/sales/history?type=daily&page=2"
                                        }
                                    },
                                    "page": {
                                        "size": 20,
                                        "total_elements": 60,
                                        "total_pages": 3
                                    }
                                });
                                break;
                        }
                    } else {
                        response.status(200).json(
                            {
                                "_embedded": {
                                    "sales_history": [{
                                        "date": "2019-12-31T09:00:00",
                                        "amount": 50000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                            "rel": "history",
                                            "method": "GET"
                                        }
                                    }, {
                                        "date": "2019-12-30T09:00:00",
                                        "amount": 50000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                            "rel": "history",
                                            "method": "GET"
                                        }
                                    }
                                        , {
                                            "date": "2019-12-29T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-28T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-27T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-26T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-25T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-24T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-23T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-22T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-21T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-22T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-20T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-19T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-18T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-17T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-16T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-15T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-14T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-13T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-12T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-11T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-10T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-09T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-08T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-07T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-06T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-05T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-04T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-05T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-04T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }, {
                                            "date": "2019-12-03T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }, {
                                            "date": "2019-12-02T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }, {
                                            "date": "2019-12-01T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                    ],
                                    "total_sales_amount": 155000
                                },
                                "_links": {
                                    "self": {
                                        "href": "/api/v1/sales/history?type=monthly&page=0"
                                    },
                                    "first": {
                                        "href": "/api/v1/sales/history?type=monthly"
                                    },
                                    "last": {
                                        "href": "/api/v1/sales/history?type=monthly&page=0"
                                    }
                                },
                                "page": {
                                    "size": 30,
                                    "total_elements": 1,
                                    "total_pages": 1
                                }
                            }
                        )
                    }
                    break;

                case 500:
                    response.sendStatus(500)
                    // response.status(500).json({
                    //     response: 'error'
                    // });
                    break;

                case 404:
                    response.sendStatus(404)
                    // response.status(404).json({
                    //     response: 'Not found'
                    // });
                    break;

                default:
                    if (type === "daily") {
                        switch (page) {
                            case "0":
                                response.status(200).json({
                                    "_embedded": {
                                        "sales_history": [
                                            {
                                                "transaction_id": "77TT2A5062K",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "VVVRL7CMWZI",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:00",
                                                "card_type": "Credit",
                                                "amount": 50000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "IQPPIL9VZH",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:07:00",
                                                "card_type": "Debit",
                                                "amount": 4500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "93PD38AGVWO",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:06:00",
                                                "card_type": "Credit",
                                                "amount": 3000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "KXAQY6ZJDV",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:05:00",
                                                "card_type": "Debit",
                                                "amount": 6500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "HFIMI967O5",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:04:00",
                                                "card_type": "Debit",
                                                "amount": 2500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "9HHQBA9ZS3T",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:03:00",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "B29MGFNIIXS",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:50",
                                                "card_type": "Debit",
                                                "amount": 9550,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "0S953DYE04LC",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:45",
                                                "card_type": "Debit",
                                                "amount": 1300,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "V7ODOSRXB3",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:35",
                                                "card_type": "Debit",
                                                "amount": 4500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "W5JQML8Y4YR",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:30",
                                                "card_type": "Debit",
                                                "amount": 15000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "EC34GDP1OZW",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:20",
                                                "card_type": "Debit",
                                                "amount": 950000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "QY6YB0JRV6G",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:15",
                                                "card_type": "Debit",
                                                "amount": 135000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "MK8B3TSCWRE",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:10",
                                                "card_type": "Debit",
                                                "amount": 7500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "WVX9REBPPT",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:03",
                                                "card_type": "Debit",
                                                "amount": 9800,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "BFHQPFK8W7T",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:01:55",
                                                "card_type": "Debit",
                                                "amount": 15600,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "H4243UGOBHN",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:01:20",
                                                "card_type": "Debit",
                                                "amount": 550000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "KZ73T10ZWZ",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:01:10",
                                                "card_type": "Debit",
                                                "amount": 98000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "5ZC1X2IVNHI",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:01:00",
                                                "card_type": "Debit",
                                                "amount": 7600,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "BQMLM296H8G",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:00:45",
                                                "card_type": "Debit",
                                                "amount": 89000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            }
                                        ],
                                        "total_sales_amount": 2169350
                                    },
                                    "_links": {
                                        "self": {
                                            "href": "/api/v1/sales/history?type=daily&page=0"
                                        },
                                        "first": {
                                            "href": "/api/v1/sales/history?type=daily"
                                        },
                                        "next": {
                                            "href": "/api/v1/sales/history?type=daily&page=1"
                                        },
                                        "last": {
                                            "href": "/api/v1/sales/history?type=daily&page=2"
                                        }
                                    },
                                    "page": {
                                        "size": 20,
                                        "total_elements": 60,
                                        "total_pages": 3
                                    }
                                })
                                break;

                            case "1":
                                response.status(200).json({
                                    "_embedded": {
                                        "sales_history": [
                                            {
                                                "transaction_id": "JQDECCKEQF",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "9ADLI08N8N6",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Credit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "X2270ECLM4B",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Credit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "NL3M5LDS8BN",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "SXJLTF1CH2E",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "5356IHY42CV",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "5G8ZJTHRX69",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "4NKPDGXDH8Q",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "BYDQVR69LDU",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "8F7GEPTPJRX",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "09V1L97PO53D",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "2Y8Y8S64A59",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "V999H90JSP",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "165FGHUK5ZT",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "UK7L8R0LBMH",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "30LUAAE8VLF",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "K4ZNDLAEFB9",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "OTA0A8BMBRN",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "VAX5PABWEM8",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "SX4ZL1S3IL",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            }
                                        ],
                                        "total_sales_amount": 2169350
                                    },
                                    "_links": {
                                        "self": {
                                            "href": "/api/v1/sales/history?type=daily&page=1"
                                        },
                                        "first": {
                                            "href": "/api/v1/sales/history?type=daily"
                                        },
                                        "prev": {
                                            "href": "/api/v1/sales/history?type=daily&page=0"
                                        },
                                        "next": {
                                            "href": "/api/v1/sales/history?type=daily&page=2"
                                        },
                                        "last": {
                                            "href": "/api/v1/sales/history?type=daily&page=2"
                                        }
                                    },
                                    "page": {
                                        "size": 20,
                                        "total_elements": 60,
                                        "total_pages": 3
                                    }
                                })
                                break;

                            case "2":
                                response.status(200).json({
                                    "_embedded": {
                                        "sales_history": [
                                            {
                                                "transaction_id": "SE5GTYT9EIK",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "2B21IU12VMH",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "VZ732B9QOJ",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "XVD8EPBLKL",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "LYTHNJU5JXN",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "VKE1J1RRMH9",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "PTGWQJKUOY",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "672SEB45WQU",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "NGC1ML3I99",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "DM5ZTMN4HJ8",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "P2TR3EH9MLR",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "4J1QZX7PIS8",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "AF8TXSB7ESF",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "DSEP98CC7G",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "AQZ6TA5QS2M",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "X0I82MQ6RLA",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "4I70ZDNX8MF",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "ZZB3RTRTQQ",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "WQKYJUZ82M",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "9W0HJWOELH",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            }
                                        ],
                                        "total_sales_amount": 2169350
                                    },
                                    "_links": {
                                        "self": {
                                            "href": "/api/v1/sales/history?type=daily&page=2"
                                        },
                                        "first": {
                                            "href": "/api/v1/sales/history?type=daily"
                                        },
                                        "prev": {
                                            "href": "/api/v1/sales/history?type=daily&page=1"
                                        },
                                        "last": {
                                            "href": "/api/v1/sales/history?type=daily&page=2"
                                        }
                                    },
                                    "page": {
                                        "size": 20,
                                        "total_elements": 60,
                                        "total_pages": 3
                                    }
                                });
                                break;

                            default:
                                response.status(200).json({
                                    "_embedded": {
                                        "sales_history": [
                                            {
                                                "transaction_id": "77TT2A5062K",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:56",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "VVVRL7CMWZI",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:08:00",
                                                "card_type": "Credit",
                                                "amount": 50000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "IQPPIL9VZH",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:07:00",
                                                "card_type": "Debit",
                                                "amount": 4500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "93PD38AGVWO",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:06:00",
                                                "card_type": "Credit",
                                                "amount": 3000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "KXAQY6ZJDV",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:05:00",
                                                "card_type": "Debit",
                                                "amount": 6500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "HFIMI967O5",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:04:00",
                                                "card_type": "Debit",
                                                "amount": 2500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "9HHQBA9ZS3T",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:03:00",
                                                "card_type": "Debit",
                                                "amount": 5000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "B29MGFNIIXS",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:50",
                                                "card_type": "Debit",
                                                "amount": 9550,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "0S953DYE04LC",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:45",
                                                "card_type": "Debit",
                                                "amount": 1300,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "V7ODOSRXB3",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:35",
                                                "card_type": "Debit",
                                                "amount": 4500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "W5JQML8Y4YR",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:30",
                                                "card_type": "Debit",
                                                "amount": 15000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "EC34GDP1OZW",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:20",
                                                "card_type": "Debit",
                                                "amount": 950000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "QY6YB0JRV6G",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:15",
                                                "card_type": "Debit",
                                                "amount": 135000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "MK8B3TSCWRE",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:10",
                                                "card_type": "Debit",
                                                "amount": 7500,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "WVX9REBPPT",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:02:03",
                                                "card_type": "Debit",
                                                "amount": 9800,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "BFHQPFK8W7T",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:01:55",
                                                "card_type": "Debit",
                                                "amount": 15600,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "H4243UGOBHN",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:01:20",
                                                "card_type": "Debit",
                                                "amount": 550000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "KZ73T10ZWZ",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:01:10",
                                                "card_type": "Debit",
                                                "amount": 98000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "5ZC1X2IVNHI",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:01:00",
                                                "card_type": "Debit",
                                                "amount": 7600,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            },
                                            {
                                                "transaction_id": "BQMLM296H8G",
                                                "authorization_code": "QWERTY1234",
                                                "date": "2001-07-04T12:00:45",
                                                "card_type": "Debit",
                                                "amount": 89000,
                                                "quantity_sales": 10,
                                                "_links": {
                                                    "href": "/api/v1/voucher/00DI59AW00O8U",
                                                    "rel": "voucher",
                                                    "method": "GET"
                                                }
                                            }
                                        ],
                                        "total_sales_amount": 2169350
                                    },
                                    "_links": {
                                        "self": {
                                            "href": "/api/v1/sales/history?type=daily&page=0"
                                        },
                                        "first": {
                                            "href": "/api/v1/sales/history?type=daily"
                                        },
                                        "next": {
                                            "href": "/api/v1/sales/history?type=daily&page=1"
                                        },
                                        "last": {
                                            "href": "/api/v1/sales/history?type=daily&page=2"
                                        }
                                    },
                                    "page": {
                                        "size": 20,
                                        "total_elements": 60,
                                        "total_pages": 3
                                    }
                                });
                                break;
                        }
                    } else {
                        response.status(200).json(
                            {
                                "_embedded": {
                                    "sales_history": [{
                                        "date": "2019-12-31T09:00:00",
                                        "amount": 50000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                            "rel": "history",
                                            "method": "GET"
                                        }
                                    }, {
                                        "date": "2019-12-30T09:00:00",
                                        "amount": 50000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                            "rel": "history",
                                            "method": "GET"
                                        }
                                    }
                                        , {
                                            "date": "2019-12-29T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-28T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-27T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-26T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-25T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-24T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-23T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-22T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-21T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-22T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-20T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-19T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-18T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-17T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-16T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-15T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-14T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-13T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-12T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-11T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-10T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-09T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-08T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-07T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-06T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-05T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-04T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-05T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                        , {
                                            "date": "2019-12-04T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }, {
                                            "date": "2019-12-03T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }, {
                                            "date": "2019-12-02T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }, {
                                            "date": "2019-12-01T09:00:00",
                                            "amount": 50000,
                                            "quantity_sales": 10,
                                            "_links": {
                                                "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                                "rel": "history",
                                                "method": "GET"
                                            }
                                        }
                                    ],
                                    "total_sales_amount": 155000
                                },
                                "_links": {
                                    "self": {
                                        "href": "/api/v1/sales/history?type=monthly&page=0"
                                    },
                                    "first": {
                                        "href": "/api/v1/sales/history?type=monthly"
                                    },
                                    "last": {
                                        "href": "/api/v1/sales/history?type=monthly&page=0"
                                    }
                                },
                                "page": {
                                    "size": 30,
                                    "total_elements": 1,
                                    "total_pages": 1
                                }
                            }
                        )
                    }
                    break;
            }
        } else {
            if (type === "daily") {
                switch (page) {
                    case "0":
                        response.status(200).json({
                            "_embedded": {
                                "sales_history": [
                                    {
                                        "transaction_id": "77TT2A5062K",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "VVVRL7CMWZI",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:00",
                                        "card_type": "Credit",
                                        "amount": 50000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "IQPPIL9VZH",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:07:00",
                                        "card_type": "Debit",
                                        "amount": 4500,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "93PD38AGVWO",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:06:00",
                                        "card_type": "Credit",
                                        "amount": 3000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "KXAQY6ZJDV",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:05:00",
                                        "card_type": "Debit",
                                        "amount": 6500,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "HFIMI967O5",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:04:00",
                                        "card_type": "Debit",
                                        "amount": 2500,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "9HHQBA9ZS3T",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:03:00",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "B29MGFNIIXS",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:02:50",
                                        "card_type": "Debit",
                                        "amount": 9550,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "0S953DYE04LC",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:02:45",
                                        "card_type": "Debit",
                                        "amount": 1300,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "V7ODOSRXB3",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:02:35",
                                        "card_type": "Debit",
                                        "amount": 4500,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "W5JQML8Y4YR",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:02:30",
                                        "card_type": "Debit",
                                        "amount": 15000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "EC34GDP1OZW",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:02:20",
                                        "card_type": "Debit",
                                        "amount": 950000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "QY6YB0JRV6G",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:02:15",
                                        "card_type": "Debit",
                                        "amount": 135000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "MK8B3TSCWRE",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:02:10",
                                        "card_type": "Debit",
                                        "amount": 7500,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "WVX9REBPPT",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:02:03",
                                        "card_type": "Debit",
                                        "amount": 9800,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "BFHQPFK8W7T",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:01:55",
                                        "card_type": "Debit",
                                        "amount": 15600,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "H4243UGOBHN",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:01:20",
                                        "card_type": "Debit",
                                        "amount": 550000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "KZ73T10ZWZ",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:01:10",
                                        "card_type": "Debit",
                                        "amount": 98000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "5ZC1X2IVNHI",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:01:00",
                                        "card_type": "Debit",
                                        "amount": 7600,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "BQMLM296H8G",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:00:45",
                                        "card_type": "Debit",
                                        "amount": 89000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    }
                                ],
                                "total_sales_amount": 2169350
                            },
                            "_links": {
                                "self": {
                                    "href": "/api/v1/sales/history?type=daily&page=0"
                                },
                                "first": {
                                    "href": "/api/v1/sales/history?type=daily"
                                },
                                "next": {
                                    "href": "/api/v1/sales/history?type=daily&page=1"
                                },
                                "last": {
                                    "href": "/api/v1/sales/history?type=daily&page=2"
                                }
                            },
                            "page": {
                                "size": 20,
                                "total_elements": 60,
                                "total_pages": 3
                            }
                        })
                        break;

                    case "1":
                        response.status(200).json({
                            "_embedded": {
                                "sales_history": [
                                    {
                                        "transaction_id": "JQDECCKEQF",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "9ADLI08N8N6",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Credit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "X2270ECLM4B",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Credit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "NL3M5LDS8BN",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "SXJLTF1CH2E",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "5356IHY42CV",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "5G8ZJTHRX69",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "4NKPDGXDH8Q",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "BYDQVR69LDU",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "8F7GEPTPJRX",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "09V1L97PO53D",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "2Y8Y8S64A59",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "V999H90JSP",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "165FGHUK5ZT",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "UK7L8R0LBMH",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "30LUAAE8VLF",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "K4ZNDLAEFB9",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "OTA0A8BMBRN",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "VAX5PABWEM8",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "SX4ZL1S3IL",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    }
                                ],
                                "total_sales_amount": 2169350
                            },
                            "_links": {
                                "self": {
                                    "href": "/api/v1/sales/history?type=daily&page=1"
                                },
                                "first": {
                                    "href": "/api/v1/sales/history?type=daily"
                                },
                                "prev": {
                                    "href": "/api/v1/sales/history?type=daily&page=0"
                                },
                                "next": {
                                    "href": "/api/v1/sales/history?type=daily&page=2"
                                },
                                "last": {
                                    "href": "/api/v1/sales/history?type=daily&page=2"
                                }
                            },
                            "page": {
                                "size": 20,
                                "total_elements": 60,
                                "total_pages": 3
                            }
                        })
                        break;

                    case "2":
                        response.status(200).json({
                            "_embedded": {
                                "sales_history": [
                                    {
                                        "transaction_id": "SE5GTYT9EIK",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "2B21IU12VMH",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "VZ732B9QOJ",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "XVD8EPBLKL",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "LYTHNJU5JXN",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "VKE1J1RRMH9",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "PTGWQJKUOY",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "672SEB45WQU",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "NGC1ML3I99",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "DM5ZTMN4HJ8",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "P2TR3EH9MLR",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "4J1QZX7PIS8",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "AF8TXSB7ESF",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "DSEP98CC7G",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "AQZ6TA5QS2M",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "X0I82MQ6RLA",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "4I70ZDNX8MF",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "ZZB3RTRTQQ",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "WQKYJUZ82M",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "9W0HJWOELH",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    }
                                ],
                                "total_sales_amount": 2169350
                            },
                            "_links": {
                                "self": {
                                    "href": "/api/v1/sales/history?type=daily&page=2"
                                },
                                "first": {
                                    "href": "/api/v1/sales/history?type=daily"
                                },
                                "prev": {
                                    "href": "/api/v1/sales/history?type=daily&page=1"
                                },
                                "last": {
                                    "href": "/api/v1/sales/history?type=daily&page=2"
                                }
                            },
                            "page": {
                                "size": 20,
                                "total_elements": 60,
                                "total_pages": 3
                            }
                        });
                        break;

                    default:
                        response.status(200).json({
                            "_embedded": {
                                "sales_history": [
                                    {
                                        "transaction_id": "77TT2A5062K",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:56",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "VVVRL7CMWZI",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:08:00",
                                        "card_type": "Credit",
                                        "amount": 50000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "IQPPIL9VZH",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:07:00",
                                        "card_type": "Debit",
                                        "amount": 4500,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "93PD38AGVWO",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:06:00",
                                        "card_type": "Credit",
                                        "amount": 3000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "KXAQY6ZJDV",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:05:00",
                                        "card_type": "Debit",
                                        "amount": 6500,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "HFIMI967O5",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:04:00",
                                        "card_type": "Debit",
                                        "amount": 2500,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "9HHQBA9ZS3T",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:03:00",
                                        "card_type": "Debit",
                                        "amount": 5000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "B29MGFNIIXS",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:02:50",
                                        "card_type": "Debit",
                                        "amount": 9550,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "0S953DYE04LC",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:02:45",
                                        "card_type": "Debit",
                                        "amount": 1300,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "V7ODOSRXB3",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:02:35",
                                        "card_type": "Debit",
                                        "amount": 4500,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "W5JQML8Y4YR",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:02:30",
                                        "card_type": "Debit",
                                        "amount": 15000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "EC34GDP1OZW",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:02:20",
                                        "card_type": "Debit",
                                        "amount": 950000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "QY6YB0JRV6G",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:02:15",
                                        "card_type": "Debit",
                                        "amount": 135000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "MK8B3TSCWRE",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:02:10",
                                        "card_type": "Debit",
                                        "amount": 7500,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "WVX9REBPPT",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:02:03",
                                        "card_type": "Debit",
                                        "amount": 9800,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "BFHQPFK8W7T",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:01:55",
                                        "card_type": "Debit",
                                        "amount": 15600,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "H4243UGOBHN",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:01:20",
                                        "card_type": "Debit",
                                        "amount": 550000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "KZ73T10ZWZ",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:01:10",
                                        "card_type": "Debit",
                                        "amount": 98000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "5ZC1X2IVNHI",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:01:00",
                                        "card_type": "Debit",
                                        "amount": 7600,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    },
                                    {
                                        "transaction_id": "BQMLM296H8G",
                                        "authorization_code": "QWERTY1234",
                                        "date": "2001-07-04T12:00:45",
                                        "card_type": "Debit",
                                        "amount": 89000,
                                        "quantity_sales": 10,
                                        "_links": {
                                            "href": "/api/v1/voucher/00DI59AW00O8U",
                                            "rel": "voucher",
                                            "method": "GET"
                                        }
                                    }
                                ],
                                "total_sales_amount": 2169350
                            },
                            "_links": {
                                "self": {
                                    "href": "/api/v1/sales/history?type=daily&page=0"
                                },
                                "first": {
                                    "href": "/api/v1/sales/history?type=daily"
                                },
                                "next": {
                                    "href": "/api/v1/sales/history?type=daily&page=1"
                                },
                                "last": {
                                    "href": "/api/v1/sales/history?type=daily&page=2"
                                }
                            },
                            "page": {
                                "size": 20,
                                "total_elements": 60,
                                "total_pages": 3
                            }
                        });
                        break;
                }
            } else {
                response.status(200).json(
                    {
                        "_embedded": {
                            "sales_history": [{
                                "date": "2019-12-31T09:00:00",
                                "amount": 50000,
                                "quantity_sales": 10,
                                "_links": {
                                    "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                    "rel": "history",
                                    "method": "GET"
                                }
                            }, {
                                "date": "2019-12-30T09:00:00",
                                "amount": 50000,
                                "quantity_sales": 10,
                                "_links": {
                                    "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                    "rel": "history",
                                    "method": "GET"
                                }
                            }
                                , {
                                    "date": "2019-12-29T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-28T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-27T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-26T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-25T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-24T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-23T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-22T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-21T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-22T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-20T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-19T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-18T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-17T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-16T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-15T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-14T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-13T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-12T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-11T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-10T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-09T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-08T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-07T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-06T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-05T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-04T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-05T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                                , {
                                    "date": "2019-12-04T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }, {
                                    "date": "2019-12-03T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }, {
                                    "date": "2019-12-02T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }, {
                                    "date": "2019-12-01T09:00:00",
                                    "amount": 50000,
                                    "quantity_sales": 10,
                                    "_links": {
                                        "href": "/api/v1/sales/history?type=daily&date=2020-01-09T09:00:00",
                                        "rel": "history",
                                        "method": "GET"
                                    }
                                }
                            ],
                            "total_sales_amount": 155000
                        },
                        "_links": {
                            "self": {
                                "href": "/api/v1/sales/history?type=monthly&page=0"
                            },
                            "first": {
                                "href": "/api/v1/sales/history?type=monthly"
                            },
                            "last": {
                                "href": "/api/v1/sales/history?type=monthly&page=0"
                            }
                        },
                        "page": {
                            "size": 30,
                            "total_elements": 1,
                            "total_pages": 1
                        }
                    }
                )
            }
        }


    } catch (e) {
        response.status(500).json(e)
    }
});
//Voucher data
// tslint:disable-next-line:no-shadowed-variable
app.get('/voucher/:transactionid', async (request, response) => {

    try {
        const transactionid = request.params.transactionid;
        const datoRef = await db.collection("transactions").doc(transactionid).get();
        let data = {};
        if (datoRef.data().amount === "12000") {
            data = {
                address: "Marín 328, Santiago",
                amount: "$12.000",
                authorization_code: 81232,
                card_type: "Credit",
                commerce_name: "Ópticas Amandas",
                date: "11/04/2018",
                hour: "12:30:23",
                operation_number: 439294,
                rut: "95.185.234-6",
                is_receipt: true,
                tip_amount: "$1.200",
                total_amount: "$13.200",
                total_number_fees: 3
            };
        } else {
            data = {
                address: "Marín 328, Santiago",
                amount: "$1.000",
                authorization_code: 81232,
                card_type: "Credit",
                commerce_name: "Ópticas Amandas",
                date: "11/04/2018",
                hour: "12:30:23",
                operation_number: 439294,
                rut: "95.185.234-6",
                is_receipt: false,
                tip_amount: "$1.200",
                total_amount: "$13.200",
                total_number_fees: 3
            };
        }
        const timer = ms => new Promise(res => setTimeout(res, ms));
        await timer(1000);
        if (globalEndpointSwitcher === "voucher") {
            switch (globalResponseStatus) {
                case 200:
                    response.status(200).json(data)
                    break;

                case 404:
                    response.sendStatus(404)
                    break;

                case 500:
                    response.sendStatus(500)
                    break;

                default:
                    response.status(200).json(data)
                    break;
            }
        } else {
            response.status(200).json(data)
        }

    } catch (e) {
        response.status(500).send(e)
    }
});
//Creates a transaction unique to a deviceid
app.post('/qr-code', async (request, response) => {

    try {
        console.log(request.body);
        // @ts-ignore
        const {deviceId, amount, tipAmount} = request.body;
        const randomOtt = (Math.random().toString(36).replace('0.', '')).toUpperCase();
        const saleAmountLength = amount.length;
        const randomOttLength = randomOtt.length;
        const total = randomOttLength + 4;
        const preQR = '00020101021227750012cl.transbank0111PAPHPHM1XXX0208999644030411099859039430513+639985903943520460165303152540' + saleAmountLength + amount + '5802CL5913Comercio Test6008Santiago81' + total + '00' + randomOttLength + randomOtt + '6304';
        const preCRC = generateChecksumCRC16(preQR);
        const totalAmount = Number(amount) + Number(tipAmount);
        let crc = preCRC.toString(16).toUpperCase();
        if (crc.length === 3) {
            crc = "0" + crc
        }
        const date = new Date();

        // @ts-ignore
        const qremv = preQR + crc;
        // 1DB5
        const data = {
            deviceId: deviceId,
            QRCodeId: randomOtt,
            transactionId: randomOtt,
            QRCodeEMV: qremv,
            amount: amount,
            totalAmount: totalAmount.toString(),
            tipAmount: tipAmount,
            totalTime: date.toISOString(),
            QRTime: 3,
            transactionTime: 4,
            ACKTime: 1,
            status: CREATED_STATUS
        };
        const datoRef = await db.collection("transactions").doc(randomOtt).set(data);
        console.log("deviceId id: " + deviceId);
        if (datoRef) {
            const timer = ms => new Promise(res => setTimeout(res, ms));
            await timer(1000);
            console.log("current global status is " + globalResponseStatus);
            if (globalEndpointSwitcher === "qr") {
                console.log("qr switcher is on");
                switch (globalResponseStatus) {
                    case 200:
                        console.log("200");
                        response.status(200).json({
                            qr_code_id: randomOtt,
                            transaction_id: randomOtt,
                            total_time: date.toISOString(),
                            qr_time: 1,
                            transaction_time: 4,
                            ack_time: 1,
                            qr_code_emv: qremv
                        });
                        break;

                    case 500:
                        console.log("500");
                        response.sendStatus(500)
                        // response.status(500).json({
                        //     response: 'error'
                        // });
                        break;

                    case 404:
                        console.log("404");
                        response.sendStatus(404)
                        // response.status(404).json({
                        //     response: 'error'
                        // });
                        break;

                    default:
                        console.log("default");
                        response.status(200).json({
                            qr_code_id: randomOtt,
                            transaction_id: randomOtt,
                            total_time: date.toISOString(),
                            qr_time: 1,
                            transaction_time: 4,
                            ack_time: 1,
                            qr_code_emv: qremv
                        });
                        break;
                }
            } else {
                console.log("not qr");
                response.status(200).json({
                    qr_code_id: randomOtt,
                    transaction_id: randomOtt,
                    total_time: date.toISOString(),
                    qr_time: 1,
                    transaction_time: 4,
                    ack_time: 1,
                    qr_code_emv: qremv
                });
            }

        }

    } catch (e) {
        console.log(e);
        response.sendStatus(500).send(e)
        // response.status(500).send(e);
    }

});
//Creates a custom error 500
app.post('/qr-code-500', async (request, response) => {
    response.status(500).json({status: "error 500"})
});
//Creates a custom error 404
app.post('/qr-code-404', async (request, response) => {
    response.status(404).json({status: "error 404"})
});
//Creates a custom error 401
app.post('/qr-code-401', async (request, response) => {
    response.status(401).json({status: "error 401"})
});
//Creates a transaction unique to a deviceid
app.post('/transaction/create', async (request, response) => {

    try {
        console.log(request.body);
        // @ts-ignore
        const {deviceId, amount, tipAmount} = request.body;
        const randomOtt = (Math.random().toString(36).replace('0.', '')).toUpperCase();
        const saleAmountLength = amount.length;
        const randomOttLength = randomOtt.length;
        const total = randomOttLength + 4;
        const preQR = '00020101021227750012cl.transbank0111PAPHPHM1XXX0208999644030411099859039430513+639985903943520460165303152540' + saleAmountLength + amount + '5802CL5913Comercio Test6008Santiago81' + total + '00' + randomOttLength + randomOtt + '6304';
        const preCRC = generateChecksumCRC16(preQR);
        let crc = preCRC.toString(16).toUpperCase();
        if (crc.length === 3) {
            crc = "0" + crc
        }
        const date = new Date();

        // @ts-ignore
        const qremv = preQR + crc;
        // 1DB5
        const data = {
            deviceId: deviceId,
            QRCodeId: randomOtt,
            transactionId: randomOtt,
            QRCodeEMV: qremv,
            amount: amount,
            totalTime: date.toISOString(),
            QRTime: 1,
            transactionTime: 4,
            ACKTime: 1,
            status: CREATED_STATUS
        };
        const datoRef = await db.collection("transactions").doc(randomOtt).set(data);
        console.log("deviceId id: " + deviceId);
        if (datoRef) {
            const timer = ms => new Promise(res => setTimeout(res, ms));
            await timer(1000);
            response.json({
                QRCodeId: randomOtt,
                transactionId: randomOtt,
                totalTime: date.toISOString(),
                QRTime: 1,
                transactionTime: 4,
                ACKTime: 1,
                QRCodeEMV: qremv
            })
        }

    } catch (e) {
        console.log(e);
        response.status(500).send(e);
    }

});
//Registers users FCM token changes
// tslint:disable-next-line:no-shadowed-variable
app.post('/setup/register', async (request, response) => {

    try {
        const {deviceid, token} = request.body;
        const data = {
            token: token,
            deviceid: deviceid
        };
        await db.collection("devices").doc(deviceid).set(data);
        response.json({
            responseCode: "OK"
        });
    } catch (e) {
        response.status(500).send(e);
    }

});

// tslint:disable-next-line:no-shadowed-variable
app.get('/reset-user', async (request, response) => {
    try {
        let code = 2
        code = request.query.code;
        const user = request.query.user;

        const usuario = user.toString().replace(".", "").replace(".", "").replace("-", "");
        const userRef = db.collection('users').doc(usuario)
        await userRef.update({
            status: code,
            loginTries: 0
        })
        response.sendStatus(200)
    } catch (e) {
        response.sendStatus(500)
    }
})

//Validates login states
// tslint:disable-next-line:no-shadowed-variable
app.post('/login', async (request, response) => {
    const activeUser = "2";
    const blockedUser = "1";
    const waitingActivationUser = "3";
    const disabledUser = "4";
    const expiredCode = "6";
    const noProduct = "7"
    const exceededLoginTries = "5"
    // const activeUser = "154893172";
    // const blockedUser = "187010578";
    // const waitingActivationUser = "123873319";
    // const disabledUser = "191309820";
    // const commonPass = "1234";
    try {
        const {user, password} = request.body;
        //
        const usuario = user.toString().replace(".", "").replace(".", "").replace("-", "");
        const userRef = db.collection('users').doc(usuario);
        const datoRef = await db.collection("users").doc(usuario).get();

        const timer = ms => new Promise(res => setTimeout(res, ms));
        await timer(1000);
        if (datoRef) {
            const userPwd = datoRef.data().password
            const userStatus = datoRef.data().status
            const loginTries = datoRef.data().loginTries
            if (loginTries >= 3) {
                response.json({
                    code: "5",
                    message: "",
                    token: "ZXCASDCASASDXC="
                });
                await userRef.update({
                    loginTries: 0,
                    status: 1
                })
                return
            }
            if (userPwd === password) {
                switch (userStatus) {
                    case activeUser:
                        await userRef.update({loginTries: 0})
                        response.json({
                            code: "2",
                            message: "",
                            token: "ZXCASDCASASDXC="
                        });
                        break;
                    case blockedUser:
                        response.json({
                            code: "1",
                            message: "",
                            token: "ZXCASDCASASDXC="
                        });
                        break;

                    case waitingActivationUser:
                        response.json({
                            code: "3",
                            message: "",
                            token: "ZXCASDCASASDXC="
                        });
                        break;
                    case disabledUser:
                        response.json({
                            code: "4",
                            message: "",
                            token: "ZXCASDCASASDXC="
                        });
                        break;
                    case expiredCode:
                        response.json({
                            code: "6",
                            message: "",
                            token: "ZXCASDCASASDXC="
                        });
                        break;
                    case noProduct:
                        response.json({
                            code: "7",
                            message: "",
                            token: "ZXCASDCASASDXC="
                        });
                        break;
                    case exceededLoginTries:
                        response.json({
                            code: "5",
                            message: "",
                            token: "ZXCASDCASASDXC="
                        });
                        break;
                    default:
                        response.json({
                            code: "0",
                            message: "",
                            token: "ZXCASDCASASDXC="
                        });
                        break;
                }
            } else {

                await userRef.update({loginTries: admin.firestore.FieldValue.increment(1)})

                response.json({
                    code: "0",
                    message: "",
                    token: "ZXCASDCASASDXC="
                })
            }
        } else {
            response.json({
                code: "0",
                message: "",
                token: "ZXCASDCASASDXC="
            })
        }

    } catch (e) {
        console.log(e);
        response.status(500).send(e);
    }

});
/**
 *
 *
 * From here onwards are custom services not meant to replicate in java
 *
 *
 *
 *
 *
 *
 */


app.get('/removeAll', async (request, response) => {

    try {
        const batch = db.batch();
        const collectionRef = db.collection('historicalsales');
        const results = await collectionRef.get();
        results.forEach(
            (doc) => {
                batch.delete(doc.ref);
            }
        );
        const commitResult = await batch.commit();
        if (commitResult) {
            response.status(200).send({responseCode: "OK"})
        }


    } catch (e) {
        response.status(500).send(e);
    }

});

app.get('/addonly/:numberositems', async (request, response) => {

    try {
        const numberositems = request.params.numberositems;
        const batch = db.batch();
        const collectionRef = db.collection('historicalsales');
        // @ts-ignore
        for (let i = 0; i < numberositems; i++) {
            const dataTemplate1Ref = {
                transactionId: "547GT67",
                saleAmount: 25000,
                authorizationCode: "81232",
                operationNumber: "23569",
                paymentMethod: "Crédito",
                paymentCredit: "Sin cuotas",
                transactionDate: "11-07-2019",
                transactionTime: "10:30"
            };
            batch.create(collectionRef.doc(), dataTemplate1Ref);
            const dataTemplate2 = {
                transactionId: "7UT8695",
                saleAmount: 4500,
                authorizationCode: "436232",
                operationNumber: "324838",
                paymentMethod: "Crédito",
                paymentCredit: "Sin cuotas",
                transactionDate: "11-07-2019",
                transactionTime: "12:30"
            };
            batch.create(collectionRef.doc(), dataTemplate2);
            const dataTemplate3 = {
                transactionId: "123TY65",
                saleAmount: 7550,
                authorizationCode: "36756",
                operationNumber: "596053",
                paymentMethod: "Crédito",
                paymentCredit: "Sin cuotas",
                transactionDate: "11-07-2019",
                transactionTime: "13:30"
            };
            batch.create(collectionRef.doc(), dataTemplate3);
            const dataTemplate4 = {
                transactionId: "90GH36",
                saleAmount: 12350,
                authorizationCode: "968483",
                operationNumber: "12360",
                paymentMethod: "Crédito",
                paymentCredit: "Sin cuotas",
                transactionDate: "11-07-2019",
                transactionTime: "14:45"
            };
            batch.create(collectionRef.doc(), dataTemplate4);
            const dataTemplate5 = {
                transactionId: "901LLK3",
                saleAmount: 950000,
                authorizationCode: "12356",
                operationNumber: "78674",
                paymentMethod: "Crédito",
                paymentCredit: "Sin cuotas",
                transactionDate: "11-07-2019",
                transactionTime: "18:09"
            };
            batch.create(collectionRef.doc(), dataTemplate5);

        }


        const result = await batch.commit();
        if (result) {
            console.log("batch written");
            response.status(200).json({responseCode: "OK"});
        }
    } catch (e) {
        response.status(500).send(e);
    }
});
// tslint:disable-next-line:no-shadowed-variable
app.get('/addfulllist', async (request, response) => {

    try {

        const batch = db.batch();
        const collectionRef = db.collection('historicalsales');
        for (let i = 0; i < 6; i++) {
            const dataTemplate1Ref = {
                transactionId: "547GT67",
                saleAmount: 25000,
                authorizationCode: "81232",
                operationNumber: "23569",
                paymentMethod: "Crédito",
                paymentCredit: "Sin cuotas",
                transactionDate: "11-07-2019",
                transactionMonth: "07",
                transactionTime: "10:30"
            };
            batch.create(collectionRef.doc(), dataTemplate1Ref);
            const dataTemplate2 = {
                transactionId: "7UT8695",
                saleAmount: 4500,
                authorizationCode: "436232",
                operationNumber: "324838",
                paymentMethod: "Crédito",
                paymentCredit: "Sin cuotas",
                transactionDate: "11-07-2019",
                transactionMonth: "07",
                transactionTime: "12:30"
            };
            batch.create(collectionRef.doc(), dataTemplate2);
            const dataTemplate3 = {
                transactionId: "123TY65",
                saleAmount: 7550,
                authorizationCode: "36756",
                operationNumber: "596053",
                paymentMethod: "Crédito",
                paymentCredit: "Sin cuotas",
                transactionDate: "10-07-2019",
                transactionMonth: "07",
                transactionTime: "13:30"
            };
            batch.create(collectionRef.doc(), dataTemplate3);
            const dataTemplate4 = {
                transactionId: "90GH36",
                saleAmount: 12350,
                authorizationCode: "968483",
                operationNumber: "12360",
                paymentMethod: "Crédito",
                paymentCredit: "Sin cuotas",
                transactionDate: "09-07-2019",
                transactionMonth: "07",
                transactionTime: "14:45"
            };
            batch.create(collectionRef.doc(), dataTemplate4);
            const dataTemplate5 = {
                transactionId: "901LLK3",
                saleAmount: 950000,
                authorizationCode: "12356",
                operationNumber: "78674",
                paymentMethod: "Crédito",
                paymentCredit: "Sin cuotas",
                transactionDate: "11-07-2019",
                transactionMonth: "07",
                transactionTime: "18:09"
            };
            batch.create(collectionRef.doc(), dataTemplate5);

        }


        const result = await batch.commit();
        if (result) {
            console.log("batch written");
            response.status(200).json({responseCode: "OK"});
        }


    } catch (e) {
        response.status(500).send(e);
    }

});

app.get('/historic/daily/:currentday/', async (request, response) => {

    try {
        const currentday = request.params.currentday;
        console.log(currentday);
        const dailyHistoricalSales = await db.collection('historicalsales')
            .where('transactionDate', '==', currentday).get();
        const arr = {
            dailySaleAmount: '',
            dailyTransactions: []
        };
        let totalSaleAmount = 0;
        dailyHistoricalSales.forEach(
            (doc) => {
                totalSaleAmount += doc.data().saleAmount;
                arr.dailyTransactions.push(doc.data())
            }
        );
        arr.dailySaleAmount = totalSaleAmount.toString();

        response.json(arr);
    } catch (e) {
        console.log(e);
        response.status(500).send(e);
    }

});

app.get('/historic/monthly/:currentmonth/', async (request, response) => {
    try {
        const currentMonth = request.params.currentmonth;
        const dailyHistoricalSales = await db.collection('historicalsales')
            .where('transactionMonth', '==', currentMonth).get();
        const arr = {
            dailySaleAmount: '',
            dailyTransactions: []
        };
        const duplicates = [];

        let totalSaleAmount = 0;
        dailyHistoricalSales.forEach(
            (doc) => {
                totalSaleAmount += doc.data().saleAmount;
                arr.dailyTransactions.push(doc.data());
                duplicates.push(doc.data());
            }
        );
        arr.dailySaleAmount = totalSaleAmount.toString();

        // let sum = {}, result;
        // let i = 0, c;
        // for (i = 0; c = duplicates[i]; ++i) {
        //     if (undefined === sum[c[0]]) {
        //         sum[c[0]] = c;
        //     } else {
        //         sum[c[0]][1] += c[1];
        //     }
        // }
        // result = Object.keys(sum).map(function (val) {
        //     return sum[val]
        // });
        const gg = {
            'monthlyTotalAmount': 2006350,
            'monthlyTransactions':
                [
                    {
                        'transactionDay': '08-07-2019',
                        'saleAmount': 1916850,
                        'transactionId': 'FGT5454G'
                    },
                    {
                        'transactionDay': '09-07-2019',
                        'saleAmount': 37050,
                        'transactionId': 'HJ675HG'
                    },
                    {
                        'transactionDay': '10-07-2019',
                        'saleAmount': 52450,
                        'transactionId': 'HJ675HG'
                    }
                ]
        };

        response.json(gg);
    } catch (e) {
        response.status(500).send(e)

    }


});

function generateChecksumCRC16(var0) {
    let var1 = 65535;
    //ValidationUtils.notNull(var0);

    let var9; //byte
    try {
        var9 = new Int8Array(new Buffer(var0));
        // var9 = var0.getBytes("UTF-8");
    } catch (var8) {
        return null;
    }

    const var2 = (var9 = var9).length;

    for (let var3 = 0; var3 < var2; ++var3) {
        const var4 = var9[var3]; //byte

        for (let var5 = 0; var5 < 8; ++var5) {
            const var6 = (var4 >> 7 - var5 & 1) === 1;
            const var7 = (var1 >> 15 & 1) === 1;
            var1 <<= 1;
            if (var7 !== var6) {
                var1 ^= 4129;
            }
        }
    }

    var1 &= 65535;
    return var1
    //return String.format("%04X", var1);
}

app.get('/update-response-status', async (request, response) => {
    try {
        globalResponseStatus = parseInt(request.query.code);
        globalEndpointSwitcher = request.query.endpoint;
        response.status(200).json({
            response: 'ok'
        });
    } catch (e) {
        console.log(e);
        response.status(500).json({
            response: e
        });
    }
});

